#!/bin/bash

twitteraccount=fsferm

if [ -z $1 ]
then 
   echo "$0 expects core data source file. Exiting..." 
   exit 1
fi

source $1

tweettext="Die #FSFE Fellowship-Gruppe Rhein/Main besucht $groupvisited in #$city am `LANG=de_DE.utf8 date -d $dateofevent +%A\,\ %d\.\ %B\ %Y` https://wiki.fsfe.org/LocalGroups/RheinMain $groupvisitedhashtag #fsferm"

# how many days left until date of event:
daysleft=$(( (`date --date $dateofevent +%s` - `date +%s`) / 86400))


if [ $daysleft -lt 7 ]
   then 
# the message to be sent:
tweettext="Nicht vergessen! #FSFE Rhein/Main zu Gast bei $groupvisited https://wiki.fsfe.org/LocalGroups/RheinMain $groupvisitedhashtag #fsferm"
fi



if [ $daysleft -lt 2 ]
   then 
# how many hours left until event:
hoursleft=$(( (`date --date "$dateofevent $meetingtime" +%s` - `date +%s`) / 3600))

# the message to be sent:
tweettext="Nicht vergessen! #FSFE Rhein/Main zu Gast bei $groupvisited https://wiki.fsfe.org/LocalGroups/RheinMain $groupvisitedhashtag #fsferm"
fi


echo "$tweettext"


exit 0





# how many days left until date of event:
daysleft=$(( (`date --date $dateofevent +%s` - `date +%s`) / 86400))

# the message to be sent:

if [ $daysleft -lt 2 ]
   then 
# how many hours left until event:
hoursleft=$(( (`date --date "$dateofevent $meetingtime" +%s` - `date +%s`) / 3600))

# the message to be sent:
fi


#echo "$diasptext"  | cliaspora -a $diaspaccount post public 
echo "$diasptext" 

exit 0
