#!/bin/bash


source $PWD/corefacts

# echo -ne "blablbabla\r"

echo -ne "BEGIN:VCALENDAR
VERSION:2.0
PRODID:-//GoofingAround/v0.01//EN
" | awk 'sub("$", "\r")'  > $PWD/fsferm.ics

for i in `find fsferm*/jobs/ -type f`

do 

source $i

echo "BEGIN:VEVENT
UID:$i-fsferm
`date  +"DTSTAMP:"%Y%m%d"T"%H%M%S"Z"`
ORGANIZER;CN=$groupname:MAILTO:rhein-main@lists.fsfe.org
DTSTAMP:`date +%Y%m%d"T"``date +%H%M%S"Z"`
DTSTART:`date -d $dateofevent  +%Y%m%d"T"``date -d $meetingtime +%H%M%S"Z"`
DTEND:`date -d $dateofevent  +%Y%m%d"T235959Z"`
SUMMARY:FSFE Rhein/Main Treffen in $city
URL:https://wiki.fsfe.org/LocalGroups/RheinMain
END:VEVENT"  | awk 'sub("$", "\r")'  >> $PWD/fsferm.ics

done

echo "END:VCALENDAR"  | awk 'sub("$", "\r")'  >> $PWD/fsferm.ics

