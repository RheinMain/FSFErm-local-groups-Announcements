#!/bin/bash

# THIS IS STILL IN DEVELOPMENT

if [ -z $1 ]
then 
   echo "$0 expects core data source file. Exiting..." 
   exit 1
fi

source $1

browser=iceweasel

echo "This is what to paste into the Fellowship wiki:"
 
echo "
##master-page:Template/FellowshipEvent
#language en
#format wiki

= Rhein-Main `date -d $dateofevent +%Y-%m-%d` =
  Start:: `date -d $dateofevent +%Y-%m-%d` $meetingtime
  End:: `date -d $dateofevent +%Y-%m-%d` 21:00
  Location:: $city, $location
  Description:: Fellowship Meeting Rhein/Main

 Topics:: See https://public.pad.fsfe.org/p/fsferm


## Separate topics with a comma (,)

[[groups/RheinMain|Details on group page]]

----
[[Category/FellowshipEvents]]"

#echo "invoke favorite browser"
#$browser https://wiki.fsfe.org/FellowshipEvents/Rhein-Main%20`date -d $dateofevent +%Y-%m-%d`?action=edit

echo
echo "____________________________________"
echo "URL for event:"
echo https://wiki.fsfe.org/Events/Rhein-Main%20`date -d $dateofevent +%Y-%m-%d`?action=edit


