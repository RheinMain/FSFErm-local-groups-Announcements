#!/bin/bash

# THIS IS STILL IN DEVELOPMENT

if [ -z $1 ]
then 
   echo "$0 expects core data source file. Exiting..." 
   exit 1
fi

source $1

browser=iceweasel

echo "This is what to paste into the Fellowship wiki:"
 
echo "
 Start:: `date -d $dateofevent +%Y-%m-%d` $meetingtime:00
 End:: `date -d $dateofevent +%Y-%m-%d` 21:00:00
 Title:: <<Verbatim(Rhein-Main `date -d $dateofevent +%Y-%m-%d`)>>
 Location:: $city, $location
 Topics:: See https://public.pad.fsfe.org/p/fsferm


Description:: Fellowship Meeting Rhein/Main
## Separate topics with a comma (,)

[[groups/RheinMain|Details on group page]]

----
CategoryFellowshipEvents"

#echo "invoke favorite browser"
#$browser https://wiki.fsfe.org/FellowshipEvents/Rhein-Main%20`date -d $dateofevent +%Y-%m-%d`?action=edit

echo "URL for event:"
echo https://wiki.fsfe.org/FellowshipEvents/Rhein-Main%20`date -d $dateofevent +%Y-%m-%d`?action=edit


