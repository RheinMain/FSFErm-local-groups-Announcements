#!/bin/bash

twitteraccount=fsferm

if [ -z $1 ]
then 
   echo "$0 expects core data source file. Exiting..." 
   exit 1
fi

source $1

tweettext="#FSFE Info-Stand auf $event in #$city am `LANG=de_DE.utf8 date -d $dateofevent +%A\,\ %d\.\ %B\ %Y` $eventurl"

# how many days left until date of event:
daysleft=$(( (`date --date $dateofevent +%s` - `date +%s`) / 86400))
 
# the message to be sent:
tweettext="Noch $daysleft Tage bis zum #FSFE Info-Stand auf $event in #$city am `LANG=de_DE.utf8 date -d $dateofevent +%A\,\ %d\.\ %B\ %Y` $eventurl"

if [ $daysleft -lt 2 ]
   then 
# how many hours left until event:
hoursleft=$(( (`date --date "$dateofevent $meetingtime" +%s` - `date +%s`) / 3600))

# the message to be sent:
tweettext="Nicht mehr lange: #FSFE Info-Stand auf $event in #$city am `LANG=de_DE.utf8 date -d $dateofevent +%A\,\ %d\.\ %B\ %Y` $eventurl"  
fi


#echo "$tweettext"

echo $tweettext  | ttytter -ssl=1 -script -keyf=$twitteraccount

exit 0
